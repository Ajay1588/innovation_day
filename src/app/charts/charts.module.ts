import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { DonutChartComponent } from './donut-chart/donut-chart.component';
// import { LineChartComponent } from './line-chart/line-chart.component';
import { PieChartsComponent } from './pie-charts/pie-charts.component';
//import { StackedAreaComponent } from './stacked-area/stacked-area.component';
// import { GaugeChartComponent } from './gauge-chart/gauge-chart.component';

@NgModule({
  declarations: [PieChartComponent, DonutChartComponent, PieChartsComponent],
  imports: [
    CommonModule
  ],
  exports: [PieChartComponent, DonutChartComponent, PieChartsComponent]
})
export class ChartsModule { }
