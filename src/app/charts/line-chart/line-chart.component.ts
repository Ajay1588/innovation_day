import { Component, OnInit, Input } from '@angular/core';
import * as Highcharts from 'highcharts';
import * as HighchartsMore from 'highcharts/highcharts-more';
import * as HighchartsSolidGauge from 'highcharts/modules/solid-gauge';

declare var require: any;
const Boost = require('highcharts/modules/boost');
const noData = require('highcharts/modules/no-data-to-display');
const More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit {

  @Input() containerName: string;

  @Input() options: any;

  highcharts = Highcharts;



  constructor() { }

  ngOnInit() {
    console.log('this.containerName line', this.options, this.containerName)
    this.createChart(this.options);
  }

  // createChart(id){
  //   var chart = Highcharts.chart(this.options);
  // }

  createChart(options){
    // options.chart.renderTo = this.containerName;
    const chart: any = Highcharts.chart(options);
    chart.renderTo = this.containerName;
  }

}
