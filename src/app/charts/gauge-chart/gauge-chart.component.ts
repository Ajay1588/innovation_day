import { Component, OnInit, Input } from '@angular/core';
//import * as Highcharts from 'highcharts';
import * as Highcharts from 'highcharts/highstock';
import * as HighchartsMore from 'highcharts/highcharts-more';
import * as HighchartsSolidGauge from 'highcharts/modules/solid-gauge';

declare var require: any;
const Boost = require('highcharts/modules/boost');
const noData = require('highcharts/modules/no-data-to-display');
const More = require('highcharts/highcharts-more');
const xrange = require('highcharts/modules/xrange');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);
xrange(Highcharts);


// // Now init modules:
// HighchartsMore(Highcharts);
// HighchartsSolidGauge(Highcharts);

@Component({
  selector: 'app-gauge-chart',
  templateUrl: './gauge-chart.component.html',
  styleUrls: ['./gauge-chart.component.scss']
})
export class GaugeChartComponent implements OnInit {

  @Input() containerName: string;
  
  @Input() options: any;

  highcharts = Highcharts;



  constructor() { }

  ngOnInit() {
    console.log('this.containerName', this.options, this.containerName)
    this.createChart(this.options);
  }

  // createChart(id){
  //   var chart = Highcharts.chart(this.options);
  // }

  createChart(options){
    // options.chart.renderTo = this.containerName;
    const chart: any = Highcharts.stockChart(options);
    chart.renderTo = this.containerName;
  }


}
