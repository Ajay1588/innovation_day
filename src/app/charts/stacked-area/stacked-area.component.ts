import { Component, OnInit, Input } from '@angular/core';
import * as Highcharts from 'highcharts';
import * as HighchartsMore from 'highcharts/highcharts-more';
import * as HighchartsSolidGauge from 'highcharts/modules/solid-gauge';

declare var require: any;
const Boost = require('highcharts/modules/boost');
const noData = require('highcharts/modules/no-data-to-display');
const More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  selector: 'app-stacked-area',
  templateUrl: './stacked-area.component.html',
  styleUrls: ['./stacked-area.component.scss']
})
export class StackedAreaComponent implements OnInit {

  
  @Input() containerName: string;
  
  @Input() options: any;

  highcharts = Highcharts; 

  constructor() { }

  ngOnInit() {
    console.log('this.containerName', this.options, this.containerName)
    this.createChart(this.options);
  }
  
  createChart(options){
    // options.chart.renderTo = this.containerName;
    const chart: any = Highcharts.chart(options);
    chart.renderTo = this.containerName;
  }


}
