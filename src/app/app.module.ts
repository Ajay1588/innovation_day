import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ToastrModule } from "ngx-toastr";

import { SidebarModule } from './sidebar/sidebar.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule} from './shared/navbar/navbar.module';
import { FixedPluginModule} from './shared/fixedplugin/fixedplugin.module';

import { AppComponent } from './app.component';
import { AppRoutes } from './app.routing';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';


// import { RecommendationComponent } from './pages/recommendation/recommendation.component';
// import { ActionConsoleComponent } from './pages/action-console/action-console.component';
// import { AdvisorDetailComponent } from './pages/advisor-detail/advisor-detail.component';
//import { AdvisorComponent } from './pages/advisor/advisor.component';
//import { EventSummaryComponent } from './pages/event-summary/event-summary.component';
//import { RecentEventsComponent } from './pages/recent-events/recent-events.component';
//import { CurrentHealthStatusComponent } from './pages/current-health-status/current-health-status.component';


@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    // RecommendationComponent,
    // ActionConsoleComponent,
    // AdvisorDetailComponent,
    //AdvisorComponent,
    //EventSummaryComponent,
    //RecentEventsComponent,
    //CurrentHealthStatusComponent
  ],
  imports: [
    BrowserAnimationsModule,
    RouterModule.forRoot(AppRoutes,{
      useHash: true
    }),
    SidebarModule,
    NavbarModule,
    ToastrModule.forRoot(),
    FooterModule,
    FixedPluginModule,
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
