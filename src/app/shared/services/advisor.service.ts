import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdvisorService {
  sharingData: any;
  counter = 1;

  passData: Subject<any>;


  constructor() {
    this.passData = new Subject();

  }

  nextCount(data) {
    this.passData.next(data);
}

  saveData(str) {
    this.sharingData = str;
  }
  getData() {
    return this.sharingData;
  }
}
