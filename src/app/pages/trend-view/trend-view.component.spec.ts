import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrendViewComponent } from './trend-view.component';

describe('TrendViewComponent', () => {
  let component: TrendViewComponent;
  let fixture: ComponentFixture<TrendViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrendViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrendViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
