import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdFanImageComponent } from './id-fan-image.component';

describe('IdFanImageComponent', () => {
  let component: IdFanImageComponent;
  let fixture: ComponentFixture<IdFanImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdFanImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdFanImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
