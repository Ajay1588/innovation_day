import { Component, OnInit } from '@angular/core';
import {AdvisorService} from '../../shared/services/advisor.service';
import { Router }  from '@angular/router'
@Component({
  selector: 'app-recommendation',
  templateUrl: './recommendation.component.html',
  styleUrls: ['./recommendation.component.scss']
})
export class RecommendationComponent implements OnInit {

  recommendationArr = []

  constructor(private advisorService: AdvisorService, private router: Router) { }

  ngOnInit() {

    const ws = new WebSocket('ws://localhost:9898/');
    if (ws) {
        console.log(ws);
    }

    ws.onmessage =  (e) => {
        // console.log(e.data)
        console.log(e.data)

        // console.log("Received: '" + JSON.parse(e.data) + "'");
      // const data = JSON.parse(e.data)
        const data = {
          'workflow': 'suggestion',
          'data': {
            'suggestionText': 'suggestionText',
            'predictedTemperature': ['predicted temperature'],
            'predictedPressure': ['predicted pressure']
          }
        }
        if (data.workflow === 'suggestion') {
          this.formatData(data.data)
        }

    }

  }

  formatData(dataFromScoket) {
    this.recommendationArr.push(dataFromScoket)
  }
  send(id) {
    // console.log(num);
    this.advisorService.nextCount(id);

  }

}
