import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-current-health-status',
  templateUrl: './current-health-status.component.html',
  styleUrls: ['./current-health-status.component.scss']
})
export class CurrentHealthStatusComponent implements OnInit {
    overAllFanHealth: any;

  constructor() { }

  ngOnInit() {
      
    this.overAllFanHealth = {
        chart: {
          zoomType: 'x',
        },
        credits: {
            enabled: false
        },
        title: {
            text: ''
        },
        xAxis: [{
          type: 'datetime',
          dateTimeLabelFormats : {
            hour: '%I %p',
            minute: '%I:%M %p'
        },
    
          min: Date.UTC(2012, 5, 22, 10, 0),
          max: Date.UTC(2012, 5, 22, 13, 30),
          tickInterval: 1800000,
          
        },{
          type: 'time',
          linkedTo: 0,
          opposite: true,
          // hidden but not visible=false:
          lineWidth: 0,
          minorGridLineWidth: 0,
          lineColor: 'transparent',
          labels: {
            enabled: false
          },
          minorTickLength: 0,
          tickLength: 0,
        }], legend: {
            reversed: true,
            align: 'right'
        },
        yAxis: [{
         // height: '17%',
         // top: '5%'
        }, {
          xAxis: 1,
          height: '45%',
          top: '30%',
          type: ['category'],
          categories: [''],
          visible: false
        }],
        series: [{
          type: 'xrange',
          showInLegend: false,
          pointPadding: 0,
          groupPadding: 0,
          xAxis: 1,
          yAxis: 1,
          dataLabels: {
            enabled: true,
            format: '{point.label}',
          },
          data: [{
            y: 0,
            x: Date.UTC(2012, 5, 22, 10, 0),
            x2: Date.UTC(2012, 5, 22, 10, 30),
            color: '#e60000',
            
         
         
          },{
            y: 0,
            x: Date.UTC(2012, 5, 22, 10, 30),
            x2: Date.UTC(2012, 5, 22, 11, 30),
            color: 'rgba(129, 241, 120, 1)',
         
          }, {
            y: 0,
            x: Date.UTC(2012, 5, 22, 11, 30),
            x2: Date.UTC(2012, 5, 22, 12, 30),
            color: '#ecec13',
            
          }, {
            y: 0,
            x: Date.UTC(2012, 5, 22, 12, 30),
            x2: Date.UTC(2012, 5, 22, 13, 0),
            color: '#e60000',
            
          }, {
            y: 0,
            x: Date.UTC(2012, 5, 22, 13, 0),
            x2: Date.UTC(2012, 5, 22, 13, 30),
            color: 'rgba(129, 241, 120, 1)',
            
          }
        
        ]
        },
        {
            name: 'Bad',
            color: '#ff4d4d',
            marker : {symbol : 'circle',radius : 12 },
            
        }, {
            name: 'Warning',
            color: '#ecec13',
            marker : {symbol : 'circle',radius : 12 },
            
        }, {
            name: 'Good',
            color: 'rgba(129, 241, 120, 1)',
            marker : {symbol : 'circle',radius : 12 },
           
        }]
      }
    
  }

}




/*{
        chart: {
            type: 'bar'
        },
        credits: {
            enabled: false
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['']
        },
        yAxis: {
            type: 'time',
            min: 0,
            title: {
                text: ''
            }
        },
        legend: {
            reversed: true,
            align: 'right'
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: 'Bad',
            color: '#ff4d4d',
            data: []
        }, {
            name: 'Warning',
            color: '#ecec13',
            data: [2]
        }, {
            name: 'Good',
            color: 'rgba(129, 241, 120, 1)',
            data: [3]
        }]
    }*/