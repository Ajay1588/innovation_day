import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentHealthStatusComponent } from './current-health-status.component';

describe('CurrentHealthStatusComponent', () => {
  let component: CurrentHealthStatusComponent;
  let fixture: ComponentFixture<CurrentHealthStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentHealthStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentHealthStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
