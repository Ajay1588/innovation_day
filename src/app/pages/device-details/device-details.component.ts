import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-device-details',
  templateUrl: './device-details.component.html',
  styleUrls: ['./device-details.component.scss']
})
export class DeviceDetailsComponent implements OnInit {

  gaugeChartPressure: any;
  gaugeChartTemperature: any;
  gaugeChartVibration: any;
  stackedAreaChartSpeed: any;
  stackedAreaChartTemperature: any;
  stackedAreaChartVibration: any;
  
  constructor() { }

  ngOnInit() {

    
    this.gaugeChartPressure = {

      chart: {
          type: 'gauge',
          plotBackgroundColor: null,
          plotBackgroundImage: null,
          plotBorderWidth: 0,
          plotShadow: false
      },
      credits: {
        enabled: false
    },
      title: {
          text: 'Inlet Pressure'
      },
  
      pane: {
          startAngle: -150,
          endAngle: 150,
          background: [{
              backgroundColor: {
                  linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                  stops: [
                      [0, '#FFF'],
                      [1, '#333']
                  ]
              },
              borderWidth: 0,
              outerRadius: '109%'
          }, {
              backgroundColor: {
                  linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                  stops: [
                      [0, '#333'],
                      [1, '#FFF']
                  ]
              },
              borderWidth: 1,
              outerRadius: '107%'
          }, {
              // default background
          }, {
              backgroundColor: '#DDD',
              borderWidth: 0,
              outerRadius: '105%',
              innerRadius: '103%'
          }]
      },
  
      // the value axis
      yAxis: {
          min: 0,
          max: 100,
  
          minorTickInterval: 'auto',
          minorTickWidth: 1,
          minorTickLength: 10,
          minorTickPosition: 'inside',
          minorTickColor: '#666',
  
          tickPixelInterval: 30,
          tickWidth: 2,
          tickPosition: 'inside',
          tickLength: 10,
          tickColor: '#666',
          labels: {
              step: 2,
              rotation: 'auto'
          },
          title: {
              text: ''
          },
          plotBands: [  {
            from: 0,
            to: 25,
            color: '#CA0B00' // green
        }, {
            from: 25,
            to: 50,
            color: '#F0D500' // yellow
        }, {
            from: 50,
            to: 75,
            color: '#00b200' // red
        }, {
            from: 75,
            to: 100,
            color: '#b5b35c'
            
        }
        ]
      },
  
      series: [{
          name: 'SpeedValue',
          data: [80],
          tooltip: {
              valueSuffix: ''
          }
      }]
  
  }

  this.gaugeChartTemperature = {

    chart: {
        type: 'gauge',
        plotBackgroundColor: null,
        plotBackgroundImage: null,
        plotBorderWidth: 0,
        plotShadow: false
    },
    credits: {
      enabled: false
  },
    title: {
        text: 'Inlet Temperature'
    },

    pane: {
        startAngle: -150,
        endAngle: 150,
        background: [{
            backgroundColor: {
                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                stops: [
                    [0, '#FFF'],
                    [1, '#333']
                ]
            },
            borderWidth: 0,
            outerRadius: '109%'
        }, {
            backgroundColor: {
                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                stops: [
                    [0, '#333'],
                    [1, '#FFF']
                ]
            },
            borderWidth: 1,
            outerRadius: '107%'
        }, {
            // default background
        }, {
            backgroundColor: '#DDD',
            borderWidth: 0,
            outerRadius: '105%',
            innerRadius: '103%'
        }]
    },

    // the value axis
    yAxis: {
        min: 0,
        max: 100,

        minorTickInterval: 'auto',
        minorTickWidth: 1,
        minorTickLength: 10,
        minorTickPosition: 'inside',
        minorTickColor: '#666',

        tickPixelInterval: 30,
        tickWidth: 2,
        tickPosition: 'inside',
        tickLength: 10,
        tickColor: '#666',
        labels: {
            step: 2,
            rotation: 'auto'
        },
        title: {
            text: ''
        },
        plotBands: [    {
          from: 0,
          to: 25,
          color: '#CA0B00' // green
      }, {
          from: 25,
          to: 50,
          color: '#F0D500' // yellow
      }, {
          from: 50,
          to: 75,
          color: '#00b200' // red
      }, {
          from: 75,
          to: 100,
          color: '#b5b35c'
          
      }]
    },

    series: [{
        name: 'Value',
        data: [80],
        tooltip: {
            valueSuffix: ''
        }
    }]

}

this.gaugeChartVibration = {

  chart: {
      type: 'gauge',
      plotBackgroundColor: null,
      plotBackgroundImage: null,
      plotBorderWidth: 0,
      plotShadow: false
  },
  credits: {
    enabled: false
},
  title: {
      text: 'Inlet Vibration'
  },

  pane: {
      startAngle: -150,
      endAngle: 150,
      background: [{
          backgroundColor: {
              linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
              stops: [
                  [0, '#FFF'],
                  [1, '#333']
              ]
          },
          borderWidth: 0,
          outerRadius: '109%'
      }, {
          backgroundColor: {
              linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
              stops: [
                  [0, '#333'],
                  [1, '#FFF']
              ]
          },
          borderWidth: 1,
          outerRadius: '107%'
      }, {
          // default background
      }, {
          backgroundColor: '#DDD',
          borderWidth: 0,
          outerRadius: '105%',
          innerRadius: '103%'
      }]
  },

  // the value axis
  yAxis: {
      min: 0,
      max: 100,

      minorTickInterval: 'auto',
      minorTickWidth: 1,
      minorTickLength: 10,
      minorTickPosition: 'inside',
      minorTickColor: '#666',

      tickPixelInterval: 30,
      tickWidth: 2,
      tickPosition: 'inside',
      tickLength: 10,
      tickColor: '#666',
      labels: {
          step: 2,
          rotation: 'auto'
      },
      title: {
          text: ''
      },
      plotBands: [     {
        from: 0,
        to: 25,
        color: '#CA0B00' // green
    }, {
        from: 25,
        to: 50,
        color: '#F0D500' // yellow
    }, {
        from: 50,
        to: 75,
        color: '#00b200' // red
    }, {
        from: 75,
        to: 100,
        color: '#b5b35c'
        
    }]
  },

  series: [{
      name: 'Value',
      data: [80],
      tooltip: {
          valueSuffix: ''
      }
  }]

}
/******************* */
this.stackedAreaChartTemperature = {

  chart: {
    type: 'area'
},
credits: {
  enabled: false
},
title: {
    text: 'Inlet Temperature And Speed Analysis'
},
subtitle: {
    text: ''
},
xAxis: {
    categories: ['2am', '4am', '6am', '8am', '10am', '12am'],
    tickmarkPlacement: 'on',
    title: {
        enabled: false
    }
},
yAxis: {
    title: {
        text: ''
    },
    labels: {
        formatter: function () {
            return this.value / 1000;
        }
    }
},
tooltip: {
    split: true,
    valueSuffix: ''
},
plotOptions: {
    area: {
        stacking: 'normal',
        lineColor: '#666666',
        lineWidth: 1,
        marker: {
            lineWidth: 1,
            lineColor: '#666666'
        }
    }
},
series: [{
  // name: '',
   data: [502, 635, 809, 947, 1402, 3634, 5268]
}, {
  // name: '',
   data: [106, 107, 111, 133, 221, 767, 1766]
}]

}

this.stackedAreaChartVibration = {

  chart: {
    type: 'area'
},
credits: {
  enabled: false
},
title: {
    text: 'Vibration'
},
subtitle: {
    text: ''
},
xAxis: {
  categories: ['2am', '4am', '6am', '8am', '10am', '12am'],
  tickmarkPlacement: 'on',
    title: {
        enabled: false
    }
},
yAxis: {
    title: {
        text: ''
    },
    labels: {
        formatter: function () {
            return this.value / 1000;
        }
    }
},
tooltip: {
    split: true,
    valueSuffix: ''
},
plotOptions: {
    area: {
        stacking: 'normal',
        lineColor: '#666666',
        lineWidth: 1,
        marker: {
            lineWidth: 1,
            lineColor: '#666666'
        }
    }
},
series: [{
   // name: '',
    data: [502, 635, 809, 947, 1402, 3634, 5268]
}, {
   // name: '',
    data: [106, 107, 111, 133, 221, 767, 1766]
}]

}

this.stackedAreaChartSpeed = {

  chart: {
    type: 'area'
},
credits: {
  enabled: false
},
title: {
    text: 'Speed'
},
subtitle: {
    text: ''
},
xAxis: {
  categories: ['2am', '4am', '6am', '8am', '10am', '12am'],
  tickmarkPlacement: 'on',
    title: {
        enabled: false
    }
},
yAxis: {
    title: {
        text: ''
    },
    labels: {
        formatter: function () {
            return this.value / 1000;
        }
    }
},
tooltip: {
    split: true,
    valueSuffix: ' '
},
plotOptions: {
    area: {
        stacking: 'normal',
        lineColor: '#666666',
        lineWidth: 1,
        marker: {
            lineWidth: 1,
            lineColor: '#666666'
        }
    }
},
series: [{
    //name: 'Asia',
    data: [502, 635, 809, 947, 1402, 3634, 5268]
}, {
    //name: 'Africa',
    data: [106, 107, 111, 133, 221, 767, 1766]
}]

}
/********************* */
  }

}
